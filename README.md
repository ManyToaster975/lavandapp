# LAVANDAPP
Es una aplicacion Android administrativa en la que se enfoca en un negocio de una Lavanderia. 

---
## Estructura del proyecto
Esta creado con el IDE Android Studio 3.1.4 usando como Base de Datos SQLITE

### Contenido
- Login para administrador y empleado

#### Para el administrador
- Seccion Clientes (Agregar, Observar, Editar y Eliminar)
- Caja 
- Seccion Inventario (Puede Agregar, editar, ver y eliminar)
- Diario
- Libreta
- Registrar entrega

#### Para el empleado
- Seccion Clientes (Agregar y editar)
- Caja 
- Seccion Inventario (Actualizar)
- Registrar entrega

---
Se necesita la APK correspondiente, se puede instalar desde la version 4.4 KitKat hasta la mas reciente.
Se necesita conexion a internet, una cuenta google y acceder a todos los permisos indicados para una mejor funcion.

package com.eagr.manytoaster.lavanderiamariela;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Inicio extends AppCompatActivity {

    private static final int NOTIF_ALERTA_ID = 0;

    ImageButton admin, empleado;
    boolean isExist = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        Notificacion();
        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT*FROM USUARIOS WHERE CLAVEUSUARIO = 1", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                isExist = true;
                //Toast.makeText(getApplicationContext(), "Existe el dato", Toast.LENGTH_SHORT).show();
            } else {
                AlertDialog.Builder opcion = new AlertDialog.Builder(Inicio.this);
                opcion.setTitle("Antes de usar");
                opcion.setMessage("Llene el siguiente formulario");
                final EditText Nombre = new EditText(Inicio.this);
                final EditText Usuario = new EditText(Inicio.this);
                final EditText Contrasena = new EditText(Inicio.this);
                //Ajuste de vistas
                Nombre.setGravity(Gravity.CENTER_HORIZONTAL);
                Nombre.setHint("Nombre Completo");
                Nombre.setInputType(InputType.TYPE_CLASS_TEXT);
                Usuario.setGravity(Gravity.CENTER_HORIZONTAL);
                Usuario.setHint("Usuario");
                Usuario.setInputType(InputType.TYPE_CLASS_TEXT);
                Contrasena.setGravity(Gravity.CENTER_HORIZONTAL);
                Contrasena.setHint("Contraseña");
                Contrasena.setInputType(InputType.TYPE_CLASS_TEXT);

                //Creacion del Layout o acomodo de los objetos
                LinearLayout acomodar = new LinearLayout(Inicio.this);
                acomodar.setOrientation(LinearLayout.VERTICAL);
                acomodar.addView(Nombre);
                acomodar.addView(Usuario);
                acomodar.addView(Contrasena);
                opcion.setView(acomodar);

                opcion.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String nom = Nombre.getText().toString();
                        String us = Usuario.getText().toString();
                        String contra = Contrasena.getText().toString();
                        if (nom.length() < 10 ) {
                            Toast.makeText(getApplicationContext(), "Inserta tu nombre completo", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), Inicio.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            finish();
                        }

                        if(us.equals("") || contra.equals("") || nom.equals("")){
                            Toast.makeText(getApplicationContext(), "Favor de llenar los campos correspondientes!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), Inicio.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            finish();
                        } else{
                            DBHelper in = new DBHelper(getApplicationContext());
                            SQLiteDatabase ise = in.getWritableDatabase();
                            ContentValues datos = new ContentValues();
                            datos.put("NOMBREUSUARIO", nom);
                            datos.put("USUARIO", us);
                            datos.put("CONTRASENA", contra);
                            ise.insert("USUARIOS", null, datos);
                            Toast.makeText(getApplicationContext(), "El Dueño ha sido registrado, bienvenido a LavandApp", Toast.LENGTH_LONG).show();
                            ise.close();
                        }
                    }
                });
                opcion.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(getApplicationContext(), Inicio.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        finish();
                    }
                });
                opcion.show();
                //Toast.makeText(getApplicationContext(), "No existe el dato", Toast.LENGTH_SHORT).show();
            }
            cursor.close();
        }

        admin = findViewById(R.id.Admin);
        empleado = findViewById(R.id.Empleado);

        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ad = new Intent(getApplicationContext(), Administrador.class);
                startActivity(ad);
            }
        });

        empleado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent em = new Intent(getApplicationContext(), Empleado.class);
                startActivity(em);
            }
        });
    }

    //Metodos
    public void Notificacion() {
        //Crea la notificacion de recordatorio del inventario
        NotificationCompat.Builder recordatorio = new NotificationCompat.Builder(Inicio.this)
                .setSmallIcon(android.R.drawable.screen_background_dark)
                .setLargeIcon((((BitmapDrawable) getResources()
                        .getDrawable(R.mipmap.logo)).getBitmap()))
                .setContentTitle("Recordatorio Inventario")
                .setContentText("Recuerda revisar el inventario de la lavanderia!")
                .setContentInfo("4")
                .setTicker("Alerta!");
        Intent notIntent =
                new Intent(Inicio.this, Inicio.class);

        PendingIntent contIntent =
                PendingIntent.getActivity(
                        Inicio.this, 0, notIntent, 0);

        recordatorio.setContentIntent(contIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(NOTIF_ALERTA_ID, recordatorio.build());
    }
}
package com.eagr.manytoaster.lavanderiamariela;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by Emanuel Garcia on 27/12/2017.
 */


//NOTA.- Al agregar, modificar o eliminar alguna tabla de la base, debes cambiar la version a utilizar.
//Al momento de cambiar la version, todos los datos de la base se mantienen en esa version
public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, "lavanderia", null, 33);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //Esto activa las llaves foraneas en SQLITE
        sqLiteDatabase.execSQL("PRAGMA foreign_keys=ON;");

        sqLiteDatabase.execSQL("CREATE TABLE USUARIOS(CLAVEUSUARIO INTEGER PRIMARY KEY AUTOINCREMENT, NOMBREUSUARIO TEXT, USUARIO TEXT, CONTRASENA TEXT)");
        //sqLiteDatabase.execSQL("INSERT INTO USUARIOS(CLAVEUSUARIO,NOMBREUSUARIO,USUARIO,CONTRASENA) VALUES(1, 'Emanuel Alejandro Garcia Ramos', 'lavanderia123', '1234')");

        sqLiteDatabase.execSQL("CREATE TABLE CLIENTES(CLAVECLIENTE INTEGER PRIMARY KEY AUTOINCREMENT, NOMBRECOMPLETOCLIENTE TEXT, TELEFONOCLIENTE INTEGER, " +
                "CORREOCLIENTE TEXT NULL)");

        sqLiteDatabase.execSQL("CREATE TABLE INVENTARIO(NUMEROPRODUCTO INTEGER PRIMARY KEY AUTOINCREMENT, NOMBREPRODUCTO TEXT, CANTIDAD INTEGER, ULTIMAFECHA TEXT)");

        sqLiteDatabase.execSQL("CREATE TABLE CAJA(FOLIO INTEGER PRIMARY KEY AUTOINCREMENT, CANTIDADCARGA TEXT, CANTIDADPIEZA TEXT, FECHAINICIO TEXT, " +
                "HORAINICIO TEXT, FECHASALIDA TEXT, HORASALIDA TEXT, PRECIOCARGA TEXT, PRECIOPIEZA TEXT, PRECIOTOTAL TEXT, " +
                "CANTIDADOTRA TEXT, PRECIOOTRO TEXT, COMENTARIO TEXT NULL, OTROSERVICIO TEXT NULL, CLAVECLIENTECAJA TEXT, FOREIGN KEY (CLAVECLIENTECAJA) REFERENCES CLIENTES(CLAVECLIENTE))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS USUARIOS");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS CLIENTES");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS INVENTARIO");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS CAJA");
        onCreate(sqLiteDatabase);
    }
}

package com.eagr.manytoaster.lavanderiamariela;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ClienteEmpleado extends AppCompatActivity {

    EditText nombre,telefono,correo;
    Button agregar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente_empleado);
        nombre=findViewById(R.id.NombreCliente);
        telefono=findViewById(R.id.TelefonoCliente);
        correo=findViewById(R.id.CorreoCliente);
        agregar=findViewById(R.id.AgregarCliente);
        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String nom=nombre.getText().toString();
                final String tel=telefono.getText().toString();
                final String cor=correo.getText().toString();
                if(nom.equals("") || tel.equals("")){
                    Toast.makeText(getApplicationContext(),"Favor de poner los datos solicitados (El correo puede ser opcional)",Toast.LENGTH_LONG).show();
                }
                else{
                    AlertDialog.Builder confirmacion=new AlertDialog.Builder(ClienteEmpleado.this);
                    confirmacion.setTitle("Confirmar Cliente");
                    confirmacion.setMessage("Estan correctos los datos?");
                    confirmacion.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            DBHelper in=new DBHelper(ClienteEmpleado.this);
                            SQLiteDatabase ise=in.getWritableDatabase();
                            ContentValues datos=new ContentValues();
                            datos.put("NOMBRECOMPLETOCLIENTE",nom);
                            datos.put("TELEFONOCLIENTE",tel);
                            datos.put("CORREOCLIENTE",cor);
                            ise.insert("CLIENTES",null,datos);
                            Toast.makeText(getApplicationContext(),"Cliente registrado!", Toast.LENGTH_SHORT).show();
                            nombre.setText("");
                            telefono.setText("");
                            correo.setText("");
                            ise.close();

                            Intent intent=ClienteEmpleado.this.getIntent();
                            ClienteEmpleado.this.finish();
                            startActivity(intent);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        }
                    });
                    confirmacion.setNegativeButton("Cancelar",null);
                    confirmacion.show();
                }
            }
        });
    }
}

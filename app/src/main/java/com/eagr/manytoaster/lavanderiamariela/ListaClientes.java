package com.eagr.manytoaster.lavanderiamariela;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListaClientes.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListaClientes#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListaClientes extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ListaClientes() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VerCliente.
     */
    // TODO: Rename and change types and number of parameters
    public static ListaClientes newInstance(String param1, String param2) {
        ListaClientes fragment = new ListaClientes();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    //------------------------------------------------------------------------------------------------------------------------------------
    /*
     * METODOS
     */
    public ArrayList llenarListaClientes(){
        DBHelper data=new DBHelper(getActivity());
        ArrayList<String> lista=new ArrayList<>();
        SQLiteDatabase database=data.getReadableDatabase();
        Cursor registro=database.rawQuery("SELECT*FROM CLIENTES",null);
        if(registro.moveToFirst()){
            do{
                //Agrega datos dependiendo del orden de los mismos
                lista.add(registro.getString(1));
            }while (registro.moveToNext());
        }
        database.close();
        return lista;
    }


    private ListView vistaCliente;
    private ArrayAdapter adaptador;
    private EditText cliente;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_lista_clientes, container, false);

        /**
         * **************************************OBJETOS*******************
         * */
        //ListView
        vistaCliente=view.findViewById(R.id.ListaClientes);

        //Ver datos de la base de datos
        adaptador=new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1,llenarListaClientes());
        vistaCliente.setAdapter(adaptador);

        cliente=view.findViewById(R.id.TextoCliente);
        cliente.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adaptador.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        vistaCliente.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Obtiene la posicion del item seleccionado
                AlertDialog.Builder verCliente=new AlertDialog.Builder(getActivity());
                final String pos=adapterView.getItemAtPosition(i).toString();
                verCliente.setTitle("Datos del cliente "+pos);

                //Agregas los campos
                final TextView clave=new TextView(getActivity());
                final TextView nombre = new TextView(getActivity());
                final TextView telefono = new TextView(getActivity());
                final TextView correo = new TextView(getActivity());

                final Button eliminar=new Button(getActivity());
                //Ajuste de vistas
                clave.setGravity(Gravity.CENTER_HORIZONTAL);
                nombre.setGravity(Gravity.CENTER_HORIZONTAL);
                telefono.setGravity(Gravity.CENTER_HORIZONTAL);
                correo.setGravity(Gravity.CENTER_HORIZONTAL);

                eliminar.setGravity(Gravity.CENTER_HORIZONTAL);
                eliminar.setText("Eliminar");
                //Creacion del Layout o acomodo de los objetos
                LinearLayout acomodar = new LinearLayout(getActivity());
                acomodar.setOrientation(LinearLayout.VERTICAL);
                acomodar.addView(clave);
                acomodar.addView(nombre);
                acomodar.addView(telefono);
                acomodar.addView(correo);
                acomodar.addView(eliminar);
                verCliente.setView(acomodar);
                //Consulta SQLITE
                DBHelper consulta = new DBHelper(getContext());
                SQLiteDatabase bd = consulta.getWritableDatabase();

                Cursor fila = bd.rawQuery("SELECT*FROM CLIENTES WHERE NOMBRECOMPLETOCLIENTE= '" + pos + "'", null);
                if (fila.moveToNext()) {
                    clave.setText("Clave: "+fila.getString(0));
                    nombre.setText("Nombre: "+fila.getString(1));
                    telefono.setText("Telefono: "+fila.getString(2));
                    correo.setText("Correo: "+fila.getString(3));
                }
                bd.close();
                verCliente.setPositiveButton("Aceptar",null);
                verCliente.show();

                eliminar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder confirmareliminar=new AlertDialog.Builder(getActivity());
                        confirmareliminar.setTitle("Aviso");
                        confirmareliminar.setMessage("Esta seguro de eliminar la informacion de este cliente?");
                        confirmareliminar.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                DBHelper el=new DBHelper(getActivity());
                                try{
                                    SQLiteDatabase eli=el.getWritableDatabase();
                                    int per;
                                    per=eli.delete("CLIENTES","NOMBRECOMPLETOCLIENTE='"+pos+"'",null);
                                    if(per==1){
                                        Toast.makeText(getContext(),"El cliente ha sido eliminado",Toast.LENGTH_SHORT).show();
                                        el.close();
                                    }
                                }
                                catch (SQLException e){
                                    Toast.makeText(getContext(),"No existe el cliente con esa clave", Toast.LENGTH_SHORT).show();
                                }
                                finally {
                                    Intent intent=getActivity().getIntent();
                                    getActivity().finish();
                                    startActivity(intent);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                }
                            }
                        });
                        confirmareliminar.setNegativeButton("Cancelar",null);
                        confirmareliminar.show();
                    }
                });
            }
        });
        return view;
    }
    //--------------------------------------------------------------------------------------------------------------------------------

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

package com.eagr.manytoaster.lavanderiamariela;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AgregarProducto.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AgregarProducto#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AgregarProducto extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AgregarProducto() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AgregarProducto.
     */
    // TODO: Rename and change types and number of parameters
    public static AgregarProducto newInstance(String param1, String param2) {
        AgregarProducto fragment = new AgregarProducto();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    //OBJETOS
    private TextView d,fecha;
    private Button insertar;
    private EditText np;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_agregar_producto, container, false);
        //Numberpicker (ADD)
        d=view.findViewById(R.id.Dato);
        //NumberPicker
        final NumberPicker cantidad=view.findViewById(R.id.cantpro);
        cantidad.setMinValue(0);
        cantidad.setMaxValue(30);
        //Sirve para que ruede la seleccion
        cantidad.setWrapSelectorWheel(true);
        cantidad.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int viejo, int nuevo) {
                d.setText(""+nuevo);
            }
        });

        fecha=view.findViewById(R.id.Fecha);
        //Obtiene la fecha
        Date data=new Date();
        final SimpleDateFormat fec=new SimpleDateFormat("d'/'MMMM'/'yyyy");
        String fechaCompleta=fec.format(data);
        fecha.setText(fechaCompleta);

        //Nombre del Producto
        np=view.findViewById(R.id.NomPro);
        insertar=view.findViewById(R.id.AdPro);
        insertar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBHelper iin=new DBHelper(getActivity());
                //Variables
                String nompro=np.getText().toString();
                int c1=Integer.parseInt(d.getText().toString());
                if(nompro.equals("")){
                    Toast.makeText(getContext(),"Favor de poner como minimo el nombre del producto", Toast.LENGTH_SHORT).show();
                }
                else{
                    String fr=fecha.getText().toString();
                    SQLiteDatabase iseIN=iin.getWritableDatabase();
                    ContentValues datos=new ContentValues();
                    //Comando para insertar
                    datos.put("NOMBREPRODUCTO",nompro);
                    datos.put("CANTIDAD", c1);
                    datos.put("ULTIMAFECHA",fr);
                    iseIN.insert("INVENTARIO",null,datos);
                    Toast.makeText(getContext(),"Producto Registrado!",Toast.LENGTH_SHORT).show();
                    np.setText("");
                    Intent intent=getActivity().getIntent();
                    getActivity().finish();
                    startActivity(intent);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                }

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

package com.eagr.manytoaster.lavanderiamariela;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListaProductos.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListaProductos#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListaProductos extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ListaProductos() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListaProductos.
     */
    // TODO: Rename and change types and number of parameters
    public static ListaProductos newInstance(String param1, String param2) {
        ListaProductos fragment = new ListaProductos();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    //Objetos
    private ListView lista;
    private ArrayAdapter ad;
    private EditText buscar;
    //Metodo
    public ArrayList llenarListaProductos() {
        DBHelper lin = new DBHelper(getActivity());
        //Declaracion valores
        ArrayList<String> ali = new ArrayList<>();
        SQLiteDatabase ci = lin.getReadableDatabase();
        //Comando o acciones
        Cursor registroInventario = ci.rawQuery("SELECT * FROM INVENTARIO", null);
        if (registroInventario.moveToFirst()) {
            do {
                ali.add(registroInventario.getString(1));
            } while (registroInventario.moveToNext());
        }
        return ali;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lista_productos, container, false);
        //Lista
        lista = view.findViewById(R.id.listaInventario);
        ad = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, llenarListaProductos());
        lista.setAdapter(ad);

        buscar=view.findViewById(R.id.TextoProducto);
        buscar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                ad.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Obtiene la posicion del item seleccionado
                AlertDialog.Builder verProducto = new AlertDialog.Builder(getActivity());
                final String pos = adapterView.getItemAtPosition(i).toString();
                verProducto.setTitle("Registro del Producto");

                //Agregas los campos
                final TextView nombreproducto = new TextView(getActivity());
                final TextView cantidad = new TextView(getActivity());
                final TextView fechaRevision = new TextView(getActivity());
                final Button actualizar = new Button(getActivity());
                //Ajuste de vistas
                nombreproducto.setGravity(Gravity.CENTER_HORIZONTAL);
                cantidad.setGravity(Gravity.CENTER_HORIZONTAL);
                fechaRevision.setGravity(Gravity.CENTER_HORIZONTAL);
                actualizar.setGravity(Gravity.CENTER_HORIZONTAL);
                actualizar.setText("Actualizar Cantidad");
                //Creacion del Layout o acomodo de los objetos
                LinearLayout acomodar = new LinearLayout(getActivity());
                acomodar.setOrientation(LinearLayout.VERTICAL);
                acomodar.addView(nombreproducto);
                acomodar.addView(cantidad);
                acomodar.addView(fechaRevision);
                acomodar.addView(actualizar);
                verProducto.setView(acomodar);
                //Consulta SQLITE
                DBHelper consulta = new DBHelper(getContext());
                SQLiteDatabase bd = consulta.getWritableDatabase();

                Cursor fila = bd.rawQuery("SELECT*FROM INVENTARIO WHERE NOMBREPRODUCTO= '" + pos + "'", null);
                if (fila.moveToNext()) {
                    nombreproducto.setText("Producto: " + fila.getString(1));
                    cantidad.setText("Cantidad existente: " + fila.getString(2));
                    fechaRevision.setText("Ultima fecha revisada: " + fila.getString(3));
                }
                bd.close();
                verProducto.setPositiveButton("Aceptar", null);
                verProducto.show();

                actualizar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final AlertDialog.Builder act = new AlertDialog.Builder(getActivity());
                        //Agregas los campos
                        final NumberPicker numeroproducto = new NumberPicker(getActivity());
                        final TextView fechanueva = new TextView(getActivity());
                        final TextView nombrecantidadNueva = new TextView(getActivity());
                        final TextView cantidadNueva = new TextView(getActivity());
                        //Ajuste de vistas
                        numeroproducto.setGravity(Gravity.CENTER_HORIZONTAL);
                        nombrecantidadNueva.setGravity(Gravity.CENTER_HORIZONTAL);
                        cantidadNueva.setGravity(Gravity.CENTER_HORIZONTAL);
                        nombrecantidadNueva.setText("Nueva cantidad: ");
                        cantidadNueva.setText("0");
                        //Creacion del Layout o acomodo de los objetos
                        LinearLayout acomodar = new LinearLayout(getActivity());
                        acomodar.setOrientation(LinearLayout.VERTICAL);
                        acomodar.addView(nombrecantidadNueva);
                        acomodar.addView(numeroproducto);
                        acomodar.addView(cantidadNueva);
                        acomodar.addView(fechanueva);
                        act.setView(acomodar);


                        numeroproducto.setMinValue(0);
                        numeroproducto.setMaxValue(30);
                        //Sirve para que ruede la seleccion
                        numeroproducto.setWrapSelectorWheel(true);
                        numeroproducto.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                            @Override
                            public void onValueChange(NumberPicker numberPicker, int viejo, int nuevo) {
                                cantidadNueva.setText("" + nuevo);
                            }
                        });

                        //Obtiene la fecha
                        Date data = new Date();
                        final SimpleDateFormat fec = new SimpleDateFormat("d'/'MMMM'/'yyyy");
                        final String fechaCompleta = fec.format(data);
                        fechanueva.setText(fechaCompleta);

                        act.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String fn = fechanueva.getText().toString();
                                DBHelper actualizacion = new DBHelper(getActivity());
                                SQLiteDatabase ac = actualizacion.getWritableDatabase();
                                //Declaracion variables
                                int c = Integer.parseInt(cantidadNueva.getText().toString());
                                ContentValues reg = new ContentValues();
                                reg.put("NUMEROPRODUCTO", pos);
                                reg.put("CANTIDAD", c);
                                reg.put("ULTIMAFECHA", fn);
                                //ComandoSQL
                                int cant = ac.update("INVENTARIO", reg, "NUMEROPRODUCTO=" + pos, null);
                                ac.close();
                                if (cant == 1) {
                                    Toast.makeText(getContext(), "Producto Actualizado!", Toast.LENGTH_SHORT).show();
                                    Intent intent=getActivity().getIntent();
                                    getActivity().finish();
                                    startActivity(intent);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                } else {
                                    Toast.makeText(getContext(), "Ese producto no existe, intente con otro", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        act.setNegativeButton("Cancelar", null);
                        act.show();
                    }
                });
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

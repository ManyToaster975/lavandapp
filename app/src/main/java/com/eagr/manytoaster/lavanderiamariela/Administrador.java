package com.eagr.manytoaster.lavanderiamariela;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Administrador extends AppCompatActivity {

    private Cursor ft;
    EditText usuarioadmin, contrasenaadmin;
    TextView claveadmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_administrador);

        claveadmin = findViewById(R.id.ClaveUsuarioAdmin);

        usuarioadmin = findViewById(R.id.UsuarioAdmin);
        contrasenaadmin = findViewById(R.id.ContrasenaAdmin);
    }

    public void acceso(View v) {
        //Clase DBHelper
        DBHelper admin = new DBHelper(this);
        SQLiteDatabase d = admin.getWritableDatabase();
        //Declaracion Variables
        String contenidoUsuario = usuarioadmin.getText().toString();
        String contenidoContrasena = contrasenaadmin.getText().toString();
        //Comando SQLite
        if (contenidoUsuario.equals("") && contenidoContrasena.equals("")) {
            Toast.makeText(getApplicationContext(), "Favor de llenar los campos correspondientes", Toast.LENGTH_SHORT).show();
        } else {
            ft = d.rawQuery("select CLAVEUSUARIO,usuario,contrasena from usuarios where usuario='" + contenidoUsuario + "' and contrasena='" + contenidoContrasena + "'", null);
            //Condicion
            if (ft.moveToFirst()) {
                //Almacenamiento de los valores en el cursor en cada variable
                String contenidoBaseClave = ft.getString(0);
                String contenidoBaseUsuario = ft.getString(1);
                String contenidoBaseContrasena = ft.getString(2);

                //Verificacion de datos
                if (contenidoUsuario.equals(contenidoBaseUsuario) && contenidoContrasena.equals(contenidoBaseContrasena) && claveadmin.getText().toString().equals(contenidoBaseClave)) {
                    Toast.makeText(getApplicationContext(), "Bienvenido " + contenidoBaseUsuario, Toast.LENGTH_SHORT).show();
                    Intent menu = new Intent(this, Menu.class);
                    startActivity(menu);
                    usuarioadmin.setText("");
                    contrasenaadmin.setText("");
                }
            }
            else {
                Toast.makeText(getApplicationContext(), "Este usuario no tiene permisos para acceder o el usuario y/o contraseña son invalidos!", Toast.LENGTH_SHORT).show();
                usuarioadmin.setText("");
                contrasenaadmin.setText("");
            }
            d.close();
        }
    }

    public void cambiarcontrasena(View v) {
        final AlertDialog.Builder cambio = new AlertDialog.Builder(Administrador.this);
        //Agregas los campos
        final EditText UsuarioCambio = new EditText(Administrador.this);
        final EditText cambiarContrasena = new EditText(Administrador.this);
        //Ajuste de vistas
        UsuarioCambio.setGravity(Gravity.CENTER_HORIZONTAL);
        UsuarioCambio.setHint("Usuario");
        UsuarioCambio.setInputType(InputType.TYPE_CLASS_TEXT);
        cambiarContrasena.setGravity(Gravity.CENTER_HORIZONTAL);
        cambiarContrasena.setHint("Contraseña nueva");
        cambiarContrasena.setInputType(InputType.TYPE_CLASS_TEXT);

        //Creacion del Layout o acomodo de los objetos
        LinearLayout acomodar = new LinearLayout(Administrador.this);
        acomodar.setOrientation(LinearLayout.VERTICAL);
        acomodar.addView(UsuarioCambio);
        acomodar.addView(cambiarContrasena);
        cambio.setView(acomodar);

        cambio.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DBHelper actualizacion = new DBHelper(getApplicationContext());
                SQLiteDatabase ac = actualizacion.getWritableDatabase();
                //Declaracion variables
                String us = UsuarioCambio.getText().toString();
                String con = cambiarContrasena.getText().toString();
                if (us.equals("") || con.equals("")) {
                    Toast.makeText(getApplicationContext(), "Favor de llenar los campos correspondientes", Toast.LENGTH_SHORT).show();
                } else {
                    ContentValues reg = new ContentValues();
                    reg.put("CLAVEUSUARIO", "1");
                    reg.put("USUARIO", us);
                    reg.put("CONTRASENA", con);
                    //ComandoSQL
                    int cant = ac.update("USUARIOS", reg, "CLAVEUSUARIO=1", null);
                    ac.close();
                    if (cant == 1) {
                        Toast.makeText(getApplicationContext(), "Contraseña actualizada!", Toast.LENGTH_SHORT).show();
                        Intent intent = Administrador.this.getIntent();
                        Administrador.this.finish();
                        startActivity(intent);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    } else {
                        Toast.makeText(getApplicationContext(), "Este usuario no existe en nuestra base de datos, intente con otra manera de escribirlo", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        cambio.setNegativeButton("Cancelar", null);
        cambio.show();
    }
}

package com.eagr.manytoaster.lavanderiamariela;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Empleado extends AppCompatActivity {

    EditText usuarioempleado, contrasenaempleado;
    private Cursor ft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empleado);
        usuarioempleado = findViewById(R.id.UsuarioEmpleado);
        contrasenaempleado = findViewById(R.id.ContrasenaEmpleado);
    }

    //Metodos
    public void accesoempleado(View v) {
        //Clase DBHelper
        DBHelper admin = new DBHelper(this);
        SQLiteDatabase d = admin.getWritableDatabase();
        //Declaracion Variables
        String contenidoUsuario = usuarioempleado.getText().toString();
        String contenidoContrasena = contrasenaempleado.getText().toString();
        //Comando SQLite
        if (contenidoContrasena.equals("") || contenidoUsuario.equals("")) {
            Toast.makeText(getApplicationContext(), "Favor de llenar los campos correspondientes", Toast.LENGTH_SHORT).show();
        } else {
            ft = d.rawQuery("select usuario,contrasena from usuarios where usuario='" + contenidoUsuario + "' and contrasena='" + contenidoContrasena + "'", null);
            //Condicion
            if (ft.moveToFirst()) {
                //Almacenamiento de los valores en el cursor en cada variable
                String contenidoBaseUsuario = ft.getString(0);
                String contenidoBaseContrasena = ft.getString(1);

                //Verificacion de datos
                if (contenidoUsuario.equals(contenidoBaseUsuario) && contenidoContrasena.equals(contenidoBaseContrasena)) {
                    Toast.makeText(getApplicationContext(), "Bienvenido " + contenidoBaseUsuario, Toast.LENGTH_SHORT).show();
                    Intent menu = new Intent(this, MenuEmpleado.class);
                    startActivity(menu);
                    usuarioempleado.setText("");
                    contrasenaempleado.setText("");
                }
            } else {
                Toast.makeText(getApplicationContext(), "El usuario y/o contraseña son invalidos!", Toast.LENGTH_SHORT).show();
                usuarioempleado.setText("");
                contrasenaempleado.setText("");
            }
            d.close();
        }
    }

    public void AgregarUsuario(View v) {
        final AlertDialog.Builder mas = new AlertDialog.Builder(Empleado.this);
        mas.setTitle("Nuevo Empleado");
        //Agregas los campos
        final EditText masNombre = new EditText(Empleado.this);
        final EditText masUsuario = new EditText(Empleado.this);
        final EditText masContra = new EditText(Empleado.this);
        //Ajuste de vistas
        masNombre.setGravity(Gravity.CENTER_HORIZONTAL);
        masNombre.setHint("Nombre");
        masNombre.setInputType(InputType.TYPE_CLASS_TEXT);
        masUsuario.setGravity(Gravity.CENTER_HORIZONTAL);
        masUsuario.setHint("Usuario");
        masUsuario.setInputType(InputType.TYPE_CLASS_TEXT);
        masContra.setGravity(Gravity.CENTER_HORIZONTAL);
        masContra.setHint("Contraseña");
        masContra.setInputType(InputType.TYPE_CLASS_TEXT);

        //Creacion del Layout o acomodo de los objetos
        LinearLayout acomodar = new LinearLayout(Empleado.this);
        acomodar.setOrientation(LinearLayout.VERTICAL);
        acomodar.addView(masNombre);
        acomodar.addView(masUsuario);
        acomodar.addView(masContra);
        mas.setView(acomodar);

        mas.setPositiveButton("Registrar Usuario", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DBHelper db = new DBHelper(Empleado.this);
                String nuevoNombre = masNombre.getText().toString();
                String nuevoUsuario = masUsuario.getText().toString();
                String nuevoContra = masContra.getText().toString();
                if (nuevoNombre.equals("") || nuevoUsuario.equals("") || nuevoContra.equals("")) {
                    Toast.makeText(getApplicationContext(), "Favor de completar todos los campos solicitados", Toast.LENGTH_SHORT).show();
                } else {
                    SQLiteDatabase AccesoEscritura = db.getWritableDatabase();
                    Cursor fila = AccesoEscritura.rawQuery("select USUARIO from USUARIOS where USUARIO='" + nuevoUsuario + "'", null);
                    if (fila.moveToFirst()) {
                        Toast.makeText(getApplicationContext(), "Este Usuario ya existe", Toast.LENGTH_SHORT).show();
                    } else {
                        ContentValues datos = new ContentValues();
                        datos.put("NOMBREUSUARIO", nuevoNombre);
                        datos.put("USUARIO", nuevoUsuario);
                        datos.put("CONTRASENA", nuevoContra);
                        AccesoEscritura.insert("USUARIOS", null, datos);
                        Toast.makeText(getApplicationContext(), "Empleado registrado!", Toast.LENGTH_SHORT).show();
                        masNombre.setText("");
                        masUsuario.setText("");
                        masContra.setText("");
                        AccesoEscritura.close();
                    }
                }
            }
        });
        mas.setNegativeButton("Salir", null);
        mas.show();
    }

    public void opciones(View v) {
        AlertDialog.Builder cambio = new AlertDialog.Builder(Empleado.this);
        cambio.setTitle("Cambio de Contraseña");
        //Agregas los campos
        final EditText UsuarioCambio = new EditText(Empleado.this);
        final EditText cambiarContrasena = new EditText(Empleado.this);
        //Ajuste de vistas
        UsuarioCambio.setGravity(Gravity.CENTER_HORIZONTAL);
        UsuarioCambio.setHint("Usuario");
        cambiarContrasena.setGravity(Gravity.CENTER_HORIZONTAL);
        cambiarContrasena.setHint("Contraseña nueva");

        //Creacion del Layout o acomodo de los objetos
        LinearLayout acomodar = new LinearLayout(Empleado.this);
        acomodar.setOrientation(LinearLayout.VERTICAL);
        acomodar.addView(UsuarioCambio);
        acomodar.addView(cambiarContrasena);
        cambio.setView(acomodar);

        cambio.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DBHelper actualizacion = new DBHelper(getApplicationContext());
                SQLiteDatabase ac = actualizacion.getWritableDatabase();
                //Declaracion variables
                String us = UsuarioCambio.getText().toString();
                String con = cambiarContrasena.getText().toString();
                ContentValues reg = new ContentValues();
                reg.put("USUARIO", us);
                reg.put("CONTRASENA", con);
                //ComandoSQL
                int cant = ac.update("USUARIOS", reg, "USUARIO='" + us + "'", null);
                ac.close();
                if (cant == 1) {
                    Toast.makeText(getApplicationContext(), "Contraseña actualizada", Toast.LENGTH_SHORT).show();
                    Intent intent = Empleado.this.getIntent();
                    Empleado.this.finish();
                    startActivity(intent);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                } else {
                    Toast.makeText(getApplicationContext(), "Este usuario no existe en nuestra base de datos, intente con otra manera de escribirlo", Toast.LENGTH_SHORT).show();
                }
            }
        });
        cambio.setNegativeButton("Cancelar", null);
        cambio.show();
    }
}

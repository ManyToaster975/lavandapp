package com.eagr.manytoaster.lavanderiamariela;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SupervisionMaquinas extends AppCompatActivity {

    boolean continuar=true;
    TextView estadoA,estadoB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supervision_maquinas);
        //Cuidado, el servidor PHP solo funciona con la API 16
        Toast.makeText(getApplicationContext(),"Actualizando datos con el servidor, espere un momento...", Toast.LENGTH_SHORT).show();

        new LavadoraA().execute();

        new LavadoraB().execute();
        Hilo();
    }

    public void Hilo(){
        Thread t=new Thread(){
            @Override
            public void run(){
                while(!isInterrupted()){
                    while (continuar){
                        try {
                            Thread.sleep(2000);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    new LavadoraA().execute();
                                    new LavadoraB().execute();
                                    //Toast.makeText(getApplicationContext(),"Actualizacion de datos", Toast.LENGTH_SHORT).show();
                                }
                            });
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
        t.start();

    }

    @Override
    public void onBackPressed(){
        continuar=false;
        Intent intent = new Intent(getApplicationContext(), Menu.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        finish();
    }

    //Lavadora A
    private class LavadoraA extends AsyncTask<Void, Void, String> {
        String JSON_URL;
        String JSON_STRING;
        @Override
        protected void onPreExecute() {
            //Recuerda que si se usa en otra lap debes quitar el 8080
            JSON_URL ="http://192.168.0.14:8080/AndroidPHP/lavadoraA.php";
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                StringBuilder JSON_DATA = new StringBuilder();
                URL url = new URL(JSON_URL);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream  in = httpURLConnection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                while ((JSON_STRING = reader.readLine())!=null) {
                    JSON_DATA.append(JSON_STRING).append("\n");
                }
                return JSON_DATA.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            estadoA=findViewById(R.id.EstadoLavadoraA);
            estadoA.setText(result);
        }
    }



    //Lavadora B
    private class LavadoraB extends AsyncTask<Void, Void, String> {
        String JSON_URL;
        String JSON_STRING;
        @Override
        protected void onPreExecute() {
            //Recuerda que si se usa en otra lap debes quitar el 8080
            JSON_URL ="http://192.168.0.14:8080/AndroidPHP/lavadoraB.php";
        }
        @Override
        protected String doInBackground(Void... params) {
            try {
                StringBuilder JSON_DATA = new StringBuilder();
                URL url = new URL(JSON_URL);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream  in = httpURLConnection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                while ((JSON_STRING = reader.readLine())!=null) {
                    JSON_DATA.append(JSON_STRING).append("\n");
                }
                return JSON_DATA.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            estadoB=findViewById(R.id.EstadoLavadoraB);
            estadoB.setText(result);
        }
    }

}

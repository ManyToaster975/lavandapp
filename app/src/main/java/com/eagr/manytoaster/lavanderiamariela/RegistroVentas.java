package com.eagr.manytoaster.lavanderiamariela;

import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class RegistroVentas extends AppCompatActivity{

    EditText folio;

    ListView registros;
    ArrayAdapter ver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_ventas);

        registros=findViewById(R.id.ListaRegistroVentas);
        ver=new ArrayAdapter(RegistroVentas.this, android.R.layout.simple_list_item_1,llenarListaPedidos());
        registros.setAdapter(ver);

        folio=findViewById(R.id.TextoFolio);
        folio.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                ver.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        registros.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AlertDialog.Builder verRegistros=new AlertDialog.Builder(RegistroVentas.this);
                final String pos=adapterView.getItemAtPosition(i).toString();
                verRegistros.setTitle("Datos del servicio");

                //Agregas los campos
                final TextView foliov = new TextView(RegistroVentas.this);
                final TextView cargav = new TextView(RegistroVentas.this);
                final TextView piezav = new TextView(RegistroVentas.this);
                final TextView fechainiciov = new TextView(RegistroVentas.this);
                final TextView horainiciov = new TextView(RegistroVentas.this);
                final TextView fechasalidav = new TextView(RegistroVentas.this);
                final TextView horasalidav = new TextView(RegistroVentas.this);
                final TextView preciocargav = new TextView(RegistroVentas.this);
                final TextView preciopiezav = new TextView(RegistroVentas.this);
                final TextView totalv = new TextView(RegistroVentas.this);
                final TextView otroserviciov=new TextView(RegistroVentas.this);
                final TextView cantidadv2 = new TextView(RegistroVentas.this);
                final TextView preciov2 = new TextView(RegistroVentas.this);
                final TextView clavev = new TextView(RegistroVentas.this);

                //Ajuste de vistas
                foliov.setGravity(Gravity.CENTER_HORIZONTAL);
                cargav.setGravity(Gravity.CENTER_HORIZONTAL);
                piezav.setGravity(Gravity.CENTER_HORIZONTAL);
                fechainiciov.setGravity(Gravity.CENTER_HORIZONTAL);
                horainiciov.setGravity(Gravity.CENTER_HORIZONTAL);
                fechasalidav.setGravity(Gravity.CENTER_HORIZONTAL);
                horasalidav.setGravity(Gravity.CENTER_HORIZONTAL);
                preciocargav.setGravity(Gravity.CENTER_HORIZONTAL);
                preciopiezav.setGravity(Gravity.CENTER_HORIZONTAL);
                totalv.setGravity(Gravity.CENTER_HORIZONTAL);
                cantidadv2.setGravity(Gravity.CENTER_HORIZONTAL);
                otroserviciov.setGravity(Gravity.CENTER_HORIZONTAL);
                preciov2.setGravity(Gravity.CENTER_HORIZONTAL);
                clavev.setGravity(Gravity.CENTER_HORIZONTAL);
                //Creacion del Layout o acomodo de los objetos
                LinearLayout acomodar = new LinearLayout(RegistroVentas.this);
                acomodar.setOrientation(LinearLayout.VERTICAL);
                acomodar.addView(foliov);
                acomodar.addView(cargav);
                acomodar.addView(piezav);
                acomodar.addView(fechainiciov);
                acomodar.addView(horainiciov);
                acomodar.addView(fechasalidav);
                acomodar.addView(horasalidav);
                acomodar.addView(preciocargav);
                acomodar.addView(preciopiezav);
                acomodar.addView(totalv);
                acomodar.addView(otroserviciov);
                acomodar.addView(cantidadv2);
                acomodar.addView(preciov2);
                acomodar.addView(clavev);
                verRegistros.setView(acomodar);
                //Consulta SQLITE
                DBHelper consulta = new DBHelper(getApplicationContext());
                SQLiteDatabase bd = consulta.getWritableDatabase();

                Cursor fila = bd.rawQuery("SELECT*FROM CAJA WHERE FOLIO= '" + pos + "'", null);
                if (fila.moveToNext()) {
                    foliov.setText("Folio: "+fila.getString(0));
                    cargav.setText("Cargas: "+fila.getString(1));
                    piezav.setText("Piezas: "+fila.getString(2));
                    fechainiciov.setText("Fecha de inicio: "+fila.getString(3));
                    horainiciov.setText("Hora de inicio: "+fila.getString(4));
                    fechasalidav.setText("Fecha de Salida: "+fila.getString(5));
                    horasalidav.setText("Hora de Salida: "+fila.getString(6));
                    preciocargav.setText("Precio Carga: $"+fila.getString(7));
                    preciopiezav.setText("Precio Pieza: $"+fila.getString(8));
                    totalv.setText("Total: $"+fila.getString(9));
                    cantidadv2.setText("Cantidad por otro Servicio: "+fila.getString(10));
                    preciov2.setText("Precio por otro Servicio: "+fila.getString(11));
                    otroserviciov.setText("Otro Servicio: "+fila.getString(13));
                    clavev.setText("Cliente (Clave): "+fila.getString(14));
                }

                verRegistros.setPositiveButton("Aceptar",null);

                verRegistros.setNeutralButton("Ver Comentario", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AlertDialog.Builder verRegistros=new AlertDialog.Builder(RegistroVentas.this);
                        verRegistros.setMessage("Comentario ");
                        final TextView com = new TextView(RegistroVentas.this);
                        //Ajuste de vistas
                        com.setGravity(Gravity.CENTER_HORIZONTAL);
                        //Creacion del Layout o acomodo de los objetos
                        LinearLayout acomodar = new LinearLayout(RegistroVentas.this);
                        acomodar.setOrientation(LinearLayout.VERTICAL);
                        acomodar.addView(com);
                        verRegistros.setView(acomodar);

                        DBHelper consulta = new DBHelper(getApplicationContext());
                        SQLiteDatabase bd = consulta.getWritableDatabase();
                        Cursor fila = bd.rawQuery("SELECT*FROM CAJA WHERE FOLIO= '" + pos + "'", null);
                        if (fila.moveToNext()) {
                            com.setText(""+fila.getString(12));
                        }
                        verRegistros.setPositiveButton("Aceptar",null);
                        verRegistros.show();
                    }
                });

                verRegistros.show();
                bd.close();
            }
        });
    }
    public ArrayList llenarListaPedidos(){
        DBHelper data=new DBHelper(RegistroVentas.this);
        ArrayList<String> lista=new ArrayList<>();
        SQLiteDatabase database=data.getReadableDatabase();
        Cursor registro=database.rawQuery("SELECT*FROM CAJA",null);
        if(registro.moveToFirst()){
            do{
                //Agrega datos dependiendo del orden de los mismos
                lista.add(registro.getString(0));
            }while (registro.moveToNext());
        }
        return lista;
    }

}

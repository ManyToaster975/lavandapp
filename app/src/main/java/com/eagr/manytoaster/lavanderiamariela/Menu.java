package com.eagr.manytoaster.lavanderiamariela;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.text.SimpleDateFormat;
import java.util.Date;

public class Menu extends AppCompatActivity {

    ImageButton cliente, inventario, caja, diario, salir, supmaq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        cliente = findViewById(R.id.Cliente);
        inventario = findViewById(R.id.Inventario);
        caja = findViewById(R.id.Pedido);
        diario = findViewById(R.id.Diario);
        supmaq = findViewById(R.id.SupervisionMaquinas);
        salir = findViewById(R.id.Cerrar);

        //Enlace a Clientes
        cliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent c = new Intent(getApplicationContext(), ClientesN.class);
                startActivity(c);
            }
        });

        //Enlace a Inventario
        inventario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(), InventarioI.class);
                startActivity(in);
            }
        });

        //Enlace a Caja
        caja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ca = new Intent(getApplicationContext(), Caja.class);
                startActivity(ca);
            }
        });

        //Cierra Sesion
        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder confirmar = new AlertDialog.Builder(Menu.this);
                confirmar.setTitle("Aviso!");
                confirmar.setMessage("Estas seguro de cerrar sesion?");
                confirmar.setPositiveButton("Si, cerrar por hoy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(getApplicationContext(), Inicio.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP); // <- Aquí :)
                        finish();
                        Toast.makeText(getApplicationContext(), "Nos vemos en otra ocasion!", Toast.LENGTH_SHORT).show();
                    }
                });
                confirmar.setNegativeButton("No, aun falta por hacer", null);
                confirmar.show();
            }
        });

        //Enlace a Supervision de Maquinas
        supmaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent su = new Intent(getApplicationContext(), SupervisionMaquinas.class);
                startActivity(su);
            }
        });

        //Enlace a Diario
        diario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent d = new Intent(getApplicationContext(), RegistroVentas.class);
                startActivity(d);
            }
        });
    }


    //Registra la hora final del pedido
    public void SalidaPedido(View v){
        AlertDialog.Builder mas=new AlertDialog.Builder(Menu.this);
        mas.setTitle("Entrega del Servicio");
        mas.setMessage("Ingrese el folio: ");
        //Agregas los campos
        final EditText folio=new EditText(Menu.this);
        final TextView fecha=new TextView(Menu.this);
        final TextView hora=new TextView(Menu.this);
        final TextView datoFecha=new TextView(Menu.this);
        final TextView datoHora=new TextView(Menu.this);
        //Ajuste de vistas
        folio.setGravity(Gravity.CENTER_HORIZONTAL);
        folio.setHint("Folio");
        folio.setInputType(InputType.TYPE_CLASS_TEXT);
        fecha.setGravity(Gravity.CENTER_HORIZONTAL);
        fecha.setText("Fecha: ");
        hora.setGravity(Gravity.CENTER_HORIZONTAL);
        hora.setText("Hora: ");
        datoFecha.setGravity(Gravity.CENTER_HORIZONTAL);
        datoHora.setGravity(Gravity.CENTER_HORIZONTAL);

        //Obtiene la fecha
        Date d=new Date();
        final SimpleDateFormat fec=new SimpleDateFormat("d'/'MMMM'/'yyyy");
        String fechaCompleta=fec.format(d);
        datoFecha.setText(fechaCompleta);

        //Obtiene la hora
        SimpleDateFormat horam=new SimpleDateFormat("h:mm a");
        String ho=horam.format(d);
        datoHora.setText(ho);


        //Creacion del Layout o acomodo de los objetos
        LinearLayout acomodar=new LinearLayout(Menu.this);
        acomodar.setOrientation(LinearLayout.VERTICAL);
        acomodar.addView(folio);
        acomodar.addView(fecha);
        acomodar.addView(datoFecha);
        acomodar.addView(hora);
        acomodar.addView(datoHora);
        mas.setView(acomodar);

        mas.setPositiveButton("Registrar Entrega", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DBHelper db=new DBHelper(Menu.this);
                String fo=folio.getText().toString();
                if(fo.equals("")){
                    Toast.makeText(getApplicationContext(),"Favor de completar todos los campos solicitados",Toast.LENGTH_SHORT).show();
                }
                else{
                    SQLiteDatabase ac=db.getWritableDatabase();
                    //Declaracion variables
                    String fs=datoFecha.getText().toString();
                    String hs=datoHora.getText().toString();
                    ContentValues reg= new ContentValues();
                    reg.put("FOLIO",fo);
                    reg.put("FECHASALIDA",fs);
                    reg.put("HORASALIDA",hs);
                    //ComandoSQL
                    int cant=ac.update("CAJA",reg,"FOLIO="+fo,null);
                    if(cant==-1){
                        Toast.makeText(getApplicationContext(),"El folio de este servicio no existe",Toast.LENGTH_SHORT).show();
                    }
                    else{
                        startActivity(new Intent(getBaseContext(),Menu.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_SINGLE_TOP));
                        Toast.makeText(getApplicationContext(),"Entrega Realizada!",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        mas.setNegativeButton("Salir", null);
        mas.show();
    }

    @Override
    public void onBackPressed(){

    }
}

package com.eagr.manytoaster.lavanderiamariela;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Caja extends AppCompatActivity {

    TextView fecha, hora, cantiadcarga, cantidadpieza, totalc, preciocarga, preciopieza, cee, pee, otroservicio;
    EditText clavecliente;
    Button lavar, secar, lavarsecar, planchar, otro, borrar, fin;

    //Cantidades en la actividad
    int lavarc = 0, secadoc = 0, lavarsec = 0, planchadoc = 0, cancarga = 0, canpieza = 0;
    float total = 0;

    //Cantidades extras
    float cantidadexta = 0, totalextra = 0;
    int precioextra = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caja);

        fecha = findViewById(R.id.Fecha);
        hora = findViewById(R.id.Hora);

        //Obtiene la fecha
        Date d = new Date();
        final SimpleDateFormat fec = new SimpleDateFormat("d'/'MMMM'/'yyyy");
        String fechaCompleta = fec.format(d);
        fecha.setText(fechaCompleta);

        //Obtiene la hora
        SimpleDateFormat horam = new SimpleDateFormat("h:mm a");
        String ho = horam.format(d);
        hora.setText(ho);

        //Clave del cliente
        clavecliente = findViewById(R.id.TextoClaveCliente);

        preciocarga = findViewById(R.id.PrecioCarga);
        preciopieza = findViewById(R.id.PrecioPieza);
        cantiadcarga = findViewById(R.id.CantidadCarga);
        cantidadpieza = findViewById(R.id.CantidadPieza);

        //Total calculada
        totalc = findViewById(R.id.Total);

        fin = findViewById(R.id.PedidoListo);

        /**
         * BOTON LAVAR
         * */
        lavar = findViewById(R.id.BotonLavado);
        lavar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lavarc = lavarc + 35;
                preciocarga.setText("" + lavarc);
                cancarga++;
                cantiadcarga.setText("" + cancarga);
                total = total + 35;
                totalc.setText("" + total);
            }
        });


        /**
         * BOTON SECAR
         * */
        secar = findViewById(R.id.BotonSecado);
        secar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                secadoc = secadoc + 35;
                preciocarga.setText("" + secadoc);
                cancarga++;
                cantiadcarga.setText("" + cancarga);
                total = total + 35;
                totalc.setText("" + total);

            }
        });
        lavarsecar = findViewById(R.id.BotonLavadoSecado);
        lavarsecar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lavarsec = lavarsec + 50;
                preciocarga.setText("" + lavarsec);
                cancarga++;
                cantiadcarga.setText("" + cancarga);
                total = total + 50;
                totalc.setText("" + total);
            }
        });


        /**
         * BOTON PLANCHAR
         * */
        planchar = findViewById(R.id.BotonPlanchado);
        planchar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                planchadoc = planchadoc + 8;
                preciopieza.setText("" + planchadoc);
                canpieza++;
                cantidadpieza.setText("" + canpieza);
                total = total + 8;
                totalc.setText("" + total);
            }
        });


        /**
         * BOTON COSAS EXTRAS
         * */
        cee = findViewById(R.id.CantidadExtra);
        pee = findViewById(R.id.PrecioExtra);
        otroservicio = findViewById(R.id.Comentario);
        cee.setText("0");
        pee.setText("0.00");
        otro = findViewById(R.id.OtrasOpciones);
        otro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder mas = new AlertDialog.Builder(Caja.this);
                mas.setTitle("Otras Opciones");
                mas.setMessage("Escriba el tipo de servicio a ofrecer: ");
                //Agregas los campos
                final EditText Servicio = new EditText(Caja.this);
                final EditText Cantidad = new EditText(Caja.this);
                final EditText Precio = new EditText(Caja.this);
                //Ajuste de vistas
                Servicio.setGravity(Gravity.CENTER_HORIZONTAL);
                Servicio.setHint("Tipo de servicio");
                Servicio.setInputType(InputType.TYPE_CLASS_TEXT);
                Cantidad.setGravity(Gravity.CENTER_HORIZONTAL);
                Cantidad.setInputType(InputType.TYPE_CLASS_NUMBER);
                Cantidad.setHint("Cantidad");
                Precio.setGravity(Gravity.CENTER_HORIZONTAL);
                Precio.setHint("Precio Individual");
                Precio.setInputType(InputType.TYPE_CLASS_NUMBER);

                //Creacion del Layout o acomodo de los objetos
                LinearLayout acomodar = new LinearLayout(Caja.this);
                acomodar.setOrientation(LinearLayout.VERTICAL);
                acomodar.addView(Servicio);
                acomodar.addView(Cantidad);
                acomodar.addView(Precio);
                mas.setView(acomodar);

                mas.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (Cantidad.equals("") || Servicio.equals("") || Precio.equals("")) {
                            Toast.makeText(Caja.this, "Favor de llenar todos los campos correspondientes", Toast.LENGTH_SHORT).show();
                        } else {
                            cantidadexta = Float.parseFloat(Cantidad.getText().toString());
                            precioextra = Integer.parseInt(Precio.getText().toString());
                            String s = Servicio.getText().toString();
                            totalextra = cantidadexta * precioextra;
                            total = total + totalextra;
                            totalc.setText("" + total);
                            cee.setText("" + Cantidad.getText().toString());
                            pee.setText("" + Precio.getText().toString());
                            otroservicio.append("\n"+Cantidad.getText().toString() + " de " + s+"  $"+precioextra+"\n");
                        }
                    }
                });
                mas.setNegativeButton("Salir", null);
                mas.show();
            }
        });


        /**
         * BOTON BORRAR
         * */
        borrar = findViewById(R.id.BotonBorrar);
        borrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder ad = new AlertDialog.Builder(Caja.this);
                ad.setTitle("Aviso!");
                ad.setMessage("Esta seguro de vaciar los datos?");
                ad.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        cantiadcarga.setText("0");
                        cantidadpieza.setText("0");
                        preciocarga.setText("0.00");
                        preciopieza.setText("0.00");
                        totalc.setText("0");
                        clavecliente.setText("");
                        lavarc = 0;
                        secadoc = 0;
                        lavarsec = 0;
                        planchadoc = 0;
                        total = 0;
                        cancarga = 0;
                        canpieza = 0;
                    }
                });
                ad.setNegativeButton("No", null);
                ad.show();
            }
        });


        /**
         * BOTON FINALIZAR SERVICIO
         * */
        fin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /**
                 * VERIFICACION DATOS CLIENTE
                 * */
                AlertDialog.Builder finn = new AlertDialog.Builder(Caja.this);
                finn.setTitle("Confirmacion");
                finn.setMessage("Esta seguro de los datos del servicio?" + "\n" + "Datos del Cliente: ");

                //Agregas los campos
                final TextView nombre = new TextView(Caja.this);
                final TextView telefono = new TextView(Caja.this);
                final TextView correo = new TextView(Caja.this);
                //Ajuste de vistas
                nombre.setGravity(Gravity.CENTER_HORIZONTAL);
                telefono.setGravity(Gravity.CENTER_HORIZONTAL);
                correo.setGravity(Gravity.CENTER_HORIZONTAL);
                //Creacion del Layout o acomodo de los objetos
                LinearLayout acomodar = new LinearLayout(Caja.this);
                acomodar.setOrientation(LinearLayout.VERTICAL);
                acomodar.addView(nombre);
                acomodar.addView(telefono);
                acomodar.addView(correo);
                finn.setView(acomodar);

                //Consulta SQLITE
                DBHelper consulta = new DBHelper(Caja.this);
                SQLiteDatabase bd = consulta.getWritableDatabase();

                Cursor fila = bd.rawQuery("SELECT*FROM CLIENTES WHERE CLAVECLIENTE= '" + clavecliente.getText().toString() + "'", null);
                if (fila.moveToNext()) {
                    nombre.setText(fila.getString(1));
                    telefono.setText(fila.getString(2));
                    correo.setText(fila.getString(3));
                }
                bd.close();

                finn.setPositiveButton("Si, esta correcto", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        /**
                         * CREAR NUEVO SERVICIO
                         * */
                        final String clave = clavecliente.getText().toString();
                        final String cc = cantiadcarga.getText().toString();
                        final String cp = cantidadpieza.getText().toString();
                        String pc = preciocarga.getText().toString();
                        String pp = preciopieza.getText().toString();
                        final String t = totalc.getText().toString();
                        final String fi = fecha.getText().toString();
                        final String hi = hora.getText().toString();
                        String hs = "No se entrega";
                        String fs = "No se entrega";
                        String otrose = otroservicio.getText().toString();
                        //Datos del extra
                        final String ce = cee.getText().toString();
                        String pe = pee.getText().toString();
                        String com = "No Hay Comentarios";

                        if(otrose.equals("")){
                            otrose="No Aplica";
                        }

                        DBHelper in = new DBHelper(Caja.this);
                        final SQLiteDatabase ise = in.getWritableDatabase();
                        //Esto activa las llaves foraneas en las clases
                        if (!ise.isReadOnly()) {
                            ise.execSQL("PRAGMA foreign_keys = ON;");
                            ContentValues datos = new ContentValues();
                            datos.put("CANTIDADCARGA", cc);
                            datos.put("CANTIDADPIEZA", cp);
                            datos.put("FECHAINICIO", fi);
                            datos.put("HORAINICIO", hi);
                            datos.put("FECHASALIDA", fs);
                            datos.put("HORASALIDA", hs);
                            datos.put("PRECIOCARGA", pc);
                            datos.put("PRECIOPIEZA", pp);
                            datos.put("PRECIOTOTAL", t);
                            datos.put("CANTIDADOTRA", ce);
                            datos.put("PRECIOOTRO", pe);
                            datos.put("COMENTARIO", com);
                            datos.put("OTROSERVICIO", otrose);
                            datos.put("CLAVECLIENTECAJA", clave);
                            //Verifica si el dato coincide con la llave foranea
                            long resultado = ise.insert("CAJA", null, datos);

                            //Verifica si se inserto la informacion a sqlite
                            if (resultado == -1) {
                                Toast.makeText(Caja.this, "El cliente no existe, favor de verificar la clave", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(Caja.this, "El servicio ha sido registrado!", Toast.LENGTH_SHORT).show();
                                clavecliente.setText("");
                                cantiadcarga.setText("0");
                                cantidadpieza.setText("0");
                                preciocarga.setText("0.00");
                                preciopieza.setText("0.00");
                                totalc.setText("0.00");


                                /**
                                 * DIALOGO COMENTARIO
                                 * */
                                DBHelper consultafolio = new DBHelper(Caja.this);
                                SQLiteDatabase bdfolio = consultafolio.getWritableDatabase();
                                Cursor fila = bdfolio.rawQuery("SELECT * FROM CAJA", null);
                                if (fila.moveToLast()) {
                                    final String f = fila.getString(0);

                                    AlertDialog.Builder finn = new AlertDialog.Builder(Caja.this);
                                    finn.setMessage("Algun aviso que tenga el cliente con la ropa que dejo encargada?");
                                    final String finalOtrose = otrose;
                                    final String finalOtrose1 = otrose;
                                    finn.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            AlertDialog.Builder finn = new AlertDialog.Builder(Caja.this);
                                            finn.setMessage("Inserte el contenido:");
                                            final EditText comentario = new EditText(Caja.this);
                                            //Ajuste de vistas
                                            comentario.setGravity(Gravity.CENTER_HORIZONTAL);

                                            //Creacion del Layout o acomodo de los objetos
                                            LinearLayout acomodar = new LinearLayout(Caja.this);
                                            acomodar.setOrientation(LinearLayout.VERTICAL);
                                            acomodar.addView(comentario);
                                            finn.setView(acomodar);

                                            finn.setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {

                                                    /**
                                                     * ACTUALIZA EL SERVICIO PARA AGREGAR UN COMENTARIO
                                                     * */
                                                    DBHelper db = new DBHelper(Caja.this);
                                                    SQLiteDatabase ac = db.getWritableDatabase();
                                                    //Declaracion variables
                                                    String coment = comentario.getText().toString();
                                                    ContentValues reg = new ContentValues();
                                                    reg.put("FOLIO", f);
                                                    reg.put("COMENTARIO", coment);
                                                    //ComandoSQL
                                                    int cant = ac.update("CAJA", reg, "FOLIO=" + f, null);
                                                    if (cant == -1) {
                                                        Toast.makeText(getApplicationContext(), "Error, no se pudo agregar la nota", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        Toast.makeText(getApplicationContext(), "Aviso adjuntado al servicio!", Toast.LENGTH_SHORT).show();
                                                        //Envia un correo al usuario con el servicio
                                                        String[] TO = {"" + correo.getText().toString()}; //aquí pon tu correo
                                                        String[] CC = {""};
                                                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                                                        emailIntent.setData(Uri.parse("mailto:"));
                                                        emailIntent.setType("text/plain");
                                                        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                                                        emailIntent.putExtra(Intent.EXTRA_CC, CC);
                                                        // Esto podrás modificarlo si quieres, el asunto y el cuerpo del mensaje
                                                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Ticket Lavanderia Mariela" + " Tel. 81214621");
                                                        emailIntent.putExtra(Intent.EXTRA_TEXT,
                                                                "Folio: " + f + "     Cliente: " + clave + "\n" +
                                                                "   Nombre del Cliente: " + nombre.getText().toString() + " " + "\n" +
                                                                "   Fecha Inicial: " + fi + "\n"+"   Hora Inicial: " + hi + "Comentario: " + coment + "\n" +
                                                                "   Cargas: " + cc + "      Piezas: " + cp + "\n"+"      Otro Servicio: " + finalOtrose + "\n" +
                                                                "   Total: $" + t + "\n" +
                                                                "FAVOR DE MOSTRAR ESTE TICKET PARA CONFIRMAR SU SERVICIO Y QUE LE SEA ENTREGADO LO QUE ENCARGO");
                                                        try {
                                                            startActivity(Intent.createChooser(emailIntent, "Enviar email..."));
                                                            Caja.this.finish();
                                                        } catch (android.content.ActivityNotFoundException ex) {
                                                            Toast.makeText(Caja.this, "Este cliente no tiene un email", Toast.LENGTH_SHORT).show();
                                                        } finally {
                                                            ise.close();
                                                        }
                                                    }
                                                }
                                            });

                                            finn.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    //Envia un correo al usuario con el servicio
                                                    String[] TO = {"" + correo.getText().toString()}; //aquí pon tu correo
                                                    String[] CC = {""};
                                                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                                                    emailIntent.setData(Uri.parse("mailto:"));
                                                    emailIntent.setType("text/plain");
                                                    emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                                                    emailIntent.putExtra(Intent.EXTRA_CC, CC);
                                                    // Esto podrás modificarlo si quieres, el asunto y el cuerpo del mensaje
                                                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Ticket Lavanderia Mariela" + " Tel. 81214621");
                                                    emailIntent.putExtra(Intent.EXTRA_TEXT, "Folio: " + f + "  Cliente: " + clave + "\n" +
                                                            "Nombre del Cliente: " + nombre.getText().toString() + " " + "\n" +
                                                            "Fecha Inicial: " + fi + " Hora Inicial: " + hi + "\n" +
                                                            "   Cargas: " + cc + "      Piezas: " + cp + "\n"+"      Otro Servicio: " + finalOtrose1 + "\n" +
                                                            " Total: $" + t + "\n" +
                                                            "FAVOR DE MOSTRAR ESTE TICKET PARA CONFIRMAR SU SERVICIO Y QUE LE SEA ENTREGADO LO QUE ENCARGO");
                                                    try {
                                                        startActivity(Intent.createChooser(emailIntent, "Enviar email..."));
                                                        Caja.this.finish();
                                                    } catch (android.content.ActivityNotFoundException ex) {
                                                        Toast.makeText(Caja.this, "Este cliente no tiene un email", Toast.LENGTH_SHORT).show();
                                                    } finally {
                                                        ise.close();
                                                    }
                                                }
                                            });
                                            finn.show();
                                        }
                                    });
                                    final String finalOtrose2 = otrose;
                                    finn.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            //Envia un correo al usuario con el servicio
                                            String[] TO = {"" + correo.getText().toString()}; //aquí pon tu correo
                                            String[] CC = {""};
                                            Intent emailIntent = new Intent(Intent.ACTION_SEND);
                                            emailIntent.setData(Uri.parse("mailto:"));
                                            emailIntent.setType("text/plain");
                                            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                                            emailIntent.putExtra(Intent.EXTRA_CC, CC);
                                            // Esto podrás modificarlo si quieres, el asunto y el cuerpo del mensaje
                                            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Ticket Lavanderia Mariela" + " Tel. 81214621");
                                            emailIntent.putExtra(Intent.EXTRA_TEXT, "Folio: " + f + "  Cliente: " + clave + "\n" +
                                                    "Nombre del Cliente: " + nombre.getText().toString() + " " + "\n" +
                                                    "Fecha Inicial: " + fi + " Hora Inicial: " + hi + "\n" +
                                                    "   Cargas: " + cc + "      Piezas: " + cp + "\n"+"      Otro Servicio: " + finalOtrose2 + "\n" +
                                                    " Total: $" + t + "\n" +
                                                    "FAVOR DE MOSTRAR ESTE TICKET PARA CONFIRMAR SU SERVICIO Y QUE LE SEA ENTREGADO LO QUE ENCARGO");
                                            try {
                                                startActivity(Intent.createChooser(emailIntent, "Enviar email..."));
                                                Caja.this.finish();
                                            } catch (android.content.ActivityNotFoundException ex) {
                                                Toast.makeText(Caja.this, "Este cliente no tiene un email", Toast.LENGTH_SHORT).show();
                                            } finally {
                                                ise.close();
                                            }
                                        }
                                    });
                                    finn.show();
                                }
                                bdfolio.close();
                            }
                        }
                    }
                });
                finn.setNegativeButton("No", null);
                finn.show();
            }
        });

    }
}
